module.exports = function(grunt){
    require('time-grunt')(grunt);
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin'
    });
   grunt.initConfig({
       sass: {
           dist: {
               files: [{
                   expand: true,
                   cwd: 'css',
                   src:['*.css'],
                   dest: 'css',
                   ext: '.css'
               }]
           }
       },
       watch: {
           files: ['css/*.scss'],
           tasks: ['css']
       },
       browserSync: {
           dev: {
               bsFiles: { //browser files
                src: [
                    'css/*.css',
                    '*.html',
                    'js/*.js'
                ]
            },
            options: {
                watchTask: true,
                server: {
                   baseDir: './' //Directorio de base para nuestro servidor
                }
            }
        }
       },
       imagemin: {
           dynamic: {
               files: [{
                   expand: true,
                   cwd: './',
                   src: 'img/*.{png,gif,jpg,jpeg}',
                   dest: 'dist/'
               }]
           }
       },
       copy: {
           html: {
               files: [{
                   expand: true,
                   dot: true,
                   cwd: './',
                   src: ['*.html'],
                   dest: 'dist'
               }]
           },
           fonts: {
               files: [
                   {
                       //para iconos como font-awesome
                       expand: true,
                       dot: true,
                       cwd: 'node-modules/@icon/open-iconic',
                       dest: 'dist'
                   }
               ]
           }
       },
       clean: {
           build: {
               src: ['dist/']
           }
       },
       cssmin: {
           dist: {}
       },
       uglify: {
           dist: {}
       },
       filerev: {
           options: {
               encoding: 'utf8',
               algorithm: 'md5',
               length: 20
           },
           release:{
            //filerev:release hashed(md5) all asset (images, js y css)
            //in dist directory
                files: [{
                    src: [
                        'dist/js/*.js',
                        'dist/css/*.css',
                    ]
                }]
           }
       },
       concat: {
           options: {
               separator: ';'
           },
           dist: {}
       },
       useminPrepare: {
           foo: {
               dest: 'dist',
               src: ['index.html', 'about.html', 'precios.html', 'contacto.html', 'terminos.html']
           },
           options: {
               flow: {
                   steps: {
                       css: ['cssmin'],
                       js: ['uglify']
                   },
                   post: {
                       css: [{
                           name: 'cssmin',
                           createConfig: function(context, block) {
                               var generated = context.options.generated;
                               generated.options = {
                                   keepSpecialComments: 0,
                                   rebase: false
                               }
                           }
                       }]
                   }
               }
           }
       },
       usemin: {
           html: ['dist/index.html', 'dist/about.html', 'dist/precios.html', 'dist/contacto.html', 'dist/terminos.html'],
           options: {
               assetDir: ['dist', 'dist/css', 'dist/js']
           }
       }
   });
   
   grunt.registerTask('css', ['sass']);
   grunt.registerTask('default', ['browserSync', 'watch']);
   grunt.registerTask('img:compress', ['imagemin']); 
   grunt.registerTask('build', [
       'clean',
       'copy',
       'imagemin',
       'useminPrepare',
       'concat',
       'cssmin',
       'ugligy',
       'filerev',
       'usemin'
   ])
};